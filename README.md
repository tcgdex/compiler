**DEPRECATED: the Compiler is now part of a dynamic server located in [the Cards Database Repository](https://github.com/tcgdex/cards-database/tree/master/server)**

# TCGdex/Compiler

_The Compiler for the [TCGdex Database](https://github.com/tcgdex/cards-database)_

# V2 Changes

## Endpoints changes

- `cards`
- `categories`
- `hp`
- `illustrators`
- `rarities`
- `retreates`
- `series` <- was `expansions`
- `sets`
- `types`

## Endpoints coming during V2 life

- `abilities`
- `abilities-type`
- `attacks`
- `attacks-by-cost`
- `attacks-by-type`
- `resistances`
- `weaknesses`
